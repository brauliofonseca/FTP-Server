#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <math.h>
#include <string.h>
#include <getopt.h>
#include "arg_functions.c"

#define ARGS "hvf:s:p:n:P:am:l:"


int main(int argc, char **argv) {
	int long_index;
	int opt;
	int socket_fd, data_conn_fd;
	char read_buffer[2048];

	if (argc < 2) {
		printf("Not enough arguments\n");
		print_help();
		exit(0);
	}

	// Setting default values
	main_args.filename = NULL;
	main_args.hostname = NULL;
	main_args.port_no = 21;
	main_args.username = "anonymous";
	main_args.password = "user@localhost.localnet";
	main_args.active_flag = 0;
	main_args.mode = "binary";
	main_args.log = NULL;


	// Create system to parse terminal command line arguments
	while((opt = getopt_long(argc, argv, ARGS, longopt, &long_index)) != -1) {
		switch(opt) {
			case 'h':
				print_help();
				exit(1);
			case 'v':
				// Print the version information
				printf("Version: 0.1\n");
				printf("Author: Braulio Fonseca\n");
				exit(1);
			case 'f':
				main_args.filename = optarg;
				break;
			case 's':
				main_args.hostname = optarg;
				break;
			case 'p':
				main_args.port_no = atoi(optarg);
				break;
			case 'n':
				main_args.username = optarg;
				break;
			case 'P':
				main_args.password = optarg;
				break;
			case 'a':
				main_args.active_flag = 1;
				break;
			case 'm':
				determine_mode(optarg);
				break;
			case 'l':
				main_args.log = optarg;
				break;
			default:
				exit(0);
		}
	}

	setup_logfile();
	socket_fd = connect_server(main_args.hostname, main_args.port_no);
	authenticate(socket_fd, main_args.username, main_args.password);
	data_conn_fd = set_pass_act_mode(socket_fd, main_args.hostname, main_args.active_flag);
	ask_for_file(socket_fd, main_args.filename);

	// First read file from FTP
	bzero(read_buffer, 2048);
	read_ftp(socket_fd, read_buffer);

	if (strncmp(read_buffer, "550 ", 4) == 0) {
		printf("File not found in the FTP server...exiting\n");
		exit(3);
	}

	printf("Passed read_ftp function\n");
	// If the file is too large, then zero out the buffer and continue to read
	while(read_buffer[2047] != 0) {
		bzero(read_buffer, 2048);
		read_ftp(socket_fd, read_buffer);

		if (strncmp(read_buffer, "550 ", 4) == 0) {
			printf("File not found in the FTP server...exiting\n");
			exit(3);
		}
	}

	write_file_to_directory(data_conn_fd, main_args.filename);


}
