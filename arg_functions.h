#ifndef ARG_FUNCTIONS_H
#define ARG_FUNCTIONS_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <getopt.h>
#include <netdb.h>
#include <arpa/inet.h>

FILE *log_file;					// File pointer for the log file
FILE *downloaded_file; 	// File pointer for the downloaded file


/* Structs to organize command line data */
struct main_args {
	char *filename;
	char *hostname;
	int port_no;
	char *username;
	char *password;
	int active_flag;
	char *mode;
	char *log;
} main_args;

/* Command line options */
const struct option longopt[] = {
	{"help", no_argument, 0, 'h' },
	{"version", no_argument, 0, 'v'},
	{"file", required_argument, 0, 'f' },
	{"server", required_argument, 0, 's' },
	{"port", required_argument, 0, 'p' },
	{"username", required_argument, 0, 'n' },
	{"password", required_argument, 0, 'P' },
	{"active", no_argument, 0, 'a' },
	{"mode", required_argument, 0, 'm' },
	{"log", required_argument, 0, 'l' },
	{0, 0, 0, 0 }
};
////////////////////////////////////////////////
// Function prototypes
////////////////////////////////////////////////

// Prints out massive help statement
void print_help();

// Determines the mode of the program from user input
void determine_mode();

// Connects to server, returns file descriptor for socket
int connect_server(char *hostname, int port_no);

// Sends username and password to the FTP server
void authenticate(int sockfd, char *username, char *password);

// Read from the FTP server response
int read_ftp(int sockfd, char *read_buffer);

// Write to the FTP serveer
int write_ftp(int sockfd, char *write_buffer);

// Send the mode specified by the user and return socket file descriptor
int set_pass_act_mode(int sockfd, char *hostname, int act_flag);

// Ask for the file
void ask_for_file(int sockfd, char *filename);

// Write file to current directory
void write_file_to_directory(int data_sock, char *filename);

// Setup function for the log file
void setup_logfile();

// End connection
void kill_all();


#endif
