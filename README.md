# FTP-Server
Simple FTP server implementation

## Compilation
gcc main.c -o mftp

## Usage
./mftp -s hostname -f ~/directory/filename


### Options
			-v 		....Prints version
			-h 		....Synopsis of application usage
			-f file 	....Specifies the file to download
			-s hostname 	....Specifies the server to download from file
			-p port 	....Specifies the port to be used when contacting server
			-n user 	....Uses the username user when logging into the FTP server (default value: anonymous)
			-P password 	....Uses the password password when logging into the FTP server (default value: user@localhost.localnet).
			-a 		....Forces active behavior (the server opens the data connection to the client) (default behavior: passive behavior).
			-m mode 	....Specifies the mode to be used for the transfer (ASCII or binary) (default value: binary).
			-l logfile 	....Logs all the FTP commands exchanged with the server and the corresponding replies to file logfile.
