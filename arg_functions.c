#include "arg_functions.h"

void print_help() {
	printf("Usage: 			mftp -s hostname -f ~/directory/filename\n");
	printf("Options:\n");
	printf("			-v 		....Prints version\n");
	printf("			-h 		....Synopsis of application usage\n");
	printf("			-f file 	....Specifies the file to download\n");
	printf("			-s hostname 	....Specifies the server to download from file\n");
	printf("			-p port 	....Specifies the port to be used when contacting server\n");
	printf("			-n user 	....Uses the username user when logging into the FTP server (default value: anonymous)\n");
	printf("			-P password 	....Uses the password password when logging into the FTP server (default value: user@localhost.localnet).\n");
	printf("			-a 		....Forces active behavior (the server opens the data connection to the client) (default behavior: passive behavior).\n");
	printf("			-m mode 	....Specifies the mode to be used for the transfer (ASCII or binary) (default value: binary).\n");
	printf("			-l logfile 	....Logs all the FTP commands exchanged with the server and the corresponding replies to file logfile.\n");
}



// Function used to catch facilitate usage of terminal input data
void determine_mode(char *option_val) {
	if (!strcmp(option_val, "binary")) {
		option_val = optarg;
	}
	else if (!strcmp(option_val, "ASCII")) {
		option_val = optarg;
	}
	else if (!strcmp(option_val, "BINARY")) {
		option_val = "binary";
	}
	else if (!strcmp(option_val, "ascii")) {
		option_val = "ASCII";
	}
	else {
		printf("Invalid entry for mode, choose ASCII or binary\n");
		exit(4);
	}
	printf("Value of main_args.mode: %s\n", option_val );
}

void write_file_to_directory(int data_sock, char *filename) {
	char read_buffer[2048];
	char *only_file_name;
	char *only_file_name_cpy;
	int counter = 1;


	while(only_file_name != NULL) {
		if (counter == 1) {
			only_file_name = strtok(filename, "/"); 	// May need to include while loop with strtok to get the last filename if a filepath is given
			printf("%s\n", only_file_name);
			counter++;
		}
		else {
			only_file_name_cpy = only_file_name;
			printf("cpy: %s\n", only_file_name_cpy);
			only_file_name = strtok(NULL, "/");
		}
	}


	downloaded_file = fopen(only_file_name_cpy, "w");

	while (read_ftp(data_sock, read_buffer) != 0) {
		fprintf(downloaded_file, "%s", read_buffer);
		bzero(read_buffer, 2048);
	}


	fclose(downloaded_file);

}




int connect_server(char *hostname, int port_no) {
	int sockfd, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	int findres;
	char buffer[1024];

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		printf("Error opening socket\n");
		exit(7);
	}

	server = gethostbyname(hostname);
	if (!server) {
		printf("Hostname could not be found\n");
		exit(2);
	}

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, 				// Fill in the struct elements of
		(char *)&serv_addr.sin_addr.s_addr, // 		serv_addr
		server->h_length);
	serv_addr.sin_port = htons(port_no); 	// Specify the port number

	if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		printf("Error connecting to server\n");
		exit(1);
	}
	return sockfd;
}



int read_ftp(int sockfd, char read_buffer[]){
	int read_num;
	read_num = read(sockfd, read_buffer, 2048);

	if (read_num < 0) {
		printf("Read is unsuccessful\n");
		return 0;
	}
	if (main_args.log != NULL) {
		if (strcmp(main_args.log, "-") == 0) {
			printf("S->C: %s", read_buffer);
		}
		else {
			fprintf(log_file, "%s", read_buffer);	// Writes to file instead of console
		}
	}
	return read_num;
}



// Function writes contents of the write buffer through socket shown
int write_ftp(int sockfd, char write_buffer[]) {
	int write_num;
	write_num = write(sockfd, write_buffer, strlen(write_buffer));
	if (write_num < 0) {
		printf("Write is unsuccessful\n");
		exit(2);
	}
	if (main_args.log != NULL) {
		if (strcmp(main_args.log, "-") == 0) {
			printf("C->S: %s", write_buffer);
		}
		else {
			fprintf(log_file, "%s", write_buffer);
		}
	}
	bzero(write_buffer, 2048);
	return write_num;
}



void authenticate(int sockfd, char *username, char *password) {
	char read_buffer[2048];
	char write_buffer[2048];
	int n;

	// Initial read from the FTP server
	read_ftp(sockfd, read_buffer);

	// Write the username and observe the response
	if (strncmp(read_buffer, "220", 3) == 0) {
		sprintf(write_buffer, "USER %s\r\n" , username);
		write_ftp(sockfd, write_buffer);
	}

	bzero(read_buffer, 2048);
	read_ftp(sockfd, read_buffer);


	if (strncmp(read_buffer, "331", 3) == 0) {
		sprintf(write_buffer, "PASS %s\r\n", password);
		write_ftp(sockfd, write_buffer);
	}

	bzero(read_buffer, 2048);
	while (strstr(read_buffer, "230 ") == NULL) {
		bzero(read_buffer, 2048);
		read_ftp(sockfd, read_buffer);
	}
}


void ask_for_file(int sockfd, char *filename) {
	char write_buffer[2048];
	sprintf(write_buffer, "RETR %s\r\n", filename);
	write_ftp(sockfd, write_buffer);
}




int set_pass_act_mode(int sockfd, char *hostname, int act_flag) {
	char read_buffer[2048];
	char write_buffer[2048];
	char *it;
	int count, pv_index, new_port, res_sockfd;
	int port_vars[2];

	if (act_flag == 0) {
		sprintf(write_buffer, "PASV\r\n");
		write_ftp(sockfd, write_buffer);

		bzero(read_buffer, 2048);
		read_ftp(sockfd, read_buffer);

		count = 0;
		pv_index = 0;
		it = strtok(read_buffer, "(),");
		while(it != NULL) {
			//printf("%s\n", it);
			if (count > 4) {
				port_vars[pv_index] = atoi(it);
				pv_index++;
			}
			count++;
			it = strtok(NULL, ".(),");
		}
		// printf("%d   %d\n", port_vars[0], port_vars[1]);
		// Calculate the port number to communicate with FTP server
		new_port = (port_vars[0] * 256) + port_vars[1];
		res_sockfd = connect_server(hostname, new_port);
		return res_sockfd;
	}
	else if (act_flag == 1) {
		printf("Entered active mode\n");
		return 0;
	}
	 return 0;
}


void setup_logfile() {
	char filename_buf[2048];

	bzero(filename_buf, 2048);
	if (main_args.log == NULL) {
		//printf("Log file is NULL\n");
		main_args.log = "-";
		//printf("Logfile val: %s\n", main_args.log);
		return;
	}
	else {
		sprintf(filename_buf, "./%s" , main_args.log);
		log_file = fopen(filename_buf, "w");
		return;
	}
}
